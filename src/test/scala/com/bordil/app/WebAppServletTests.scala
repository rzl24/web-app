package com.bordil.app

import org.scalatra.test.scalatest._

class WebAppServletTests extends ScalatraFunSuite {

  addServlet(classOf[WebAppServlet], "/*")

  test("GET / on WebAppServlet should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
